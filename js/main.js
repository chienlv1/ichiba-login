function login_slide () {
    var swiper = new Swiper('.login_slide .swiper-container', {
        pagination: {
            el: '.swiper-pagination',
        },
    });
}
function slideToUnclock (){
    //Demo https://www.jqueryscript.net/slider/Slide-Swipe-To-Unlock-Slider.html
    $(".form_slide_unlock #slide_unlock").slideToUnlock({
        // default
        lockText:'Trượt để nhận mã xác thực',
        unlockText:'Cám ơn bạn',
        unlockfn:function(){console.log("unlock")},
        lockfn  :function(){},

});

}
$(document).ready(function () {
    login_slide();
    slideToUnclock();
});